package org.fjala.progra2.dictionary;

import java.util.*;

public class MyHashTable<K, V> extends Dictionary<K, V> {

    private static final int DEFAULT_DATA_LENGTH = 13;
    private final Entry<K, V>[] table;
    private int size;

    public MyHashTable() {
        this(DEFAULT_DATA_LENGTH);
    }

    @SuppressWarnings("unchecked")
    public MyHashTable(int initialLength) {
        this.table = new Entry[initialLength];
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public Enumeration<K> keys() {
        return new KeyEnumerator();
    }


    @Override
    public Enumeration<V> elements() {
        return new ValueEnumerator();
    }

    public Enumeration<Entry<K, V>> entries() {
        return new EntryEnumerator();
    }

    @Override
    public V get(Object key) {
        Entry<K, V> entry = getEntry(key);
        return entry == null ? null : entry.value;
    }

    private Entry<K, V> getEntry(Object key) {
        int hash = hash(key);
        Entry<K, V> node= table[hash];
        if (node != null) {
            do {
                if (node.key.equals(key)) {
                    return node;
                }
                node = node.next;
            } while (node != null);
        }
        return null;
    }

    @Override
    public V put(K key, V value) {
        int hash = hash(key);
        Entry<K, V> node = new Entry<>(key, value);
        return putInHash(hash, node);
    }

    private V putInHash(int hash, Entry<K,V> newNode) {
        Entry<K,V> node = table[hash];
        if (node == null) {
            table[hash] = newNode;
        } else {
            do {
                if (node.key.equals(newNode.key)) {
                    System.out.print("Reemplazando valor ");
                    System.out.println(node + " a " + newNode);
                    V previous = node.value;
                    node.setValue(newNode.value);
                    return previous;
                }
                if (node.hasNext())
                    node = node.next;
            } while (node.hasNext());
            node.next = newNode;
        }
        size++;
        return null;
    }


    private int hash(Object k) {
        int hash = k.hashCode();
        return (hash & 0x7fffffff) % table.length;
    }

    @Override
    public V remove(Object key) {
        int hash = hash(key);
        Entry<K, V> node = table[hash];
        if (node != null) {
            if(node.key == key) {
                V previous = node.value;
                table[hash] = node.next;
                size--;
                return previous;
            }
            while (node.next != null) {
                if (node.next.key.equals(key)) {
                    V previous = node.next.value;
                    node.next = node.next.next;
                    size--;
                    return previous;
                }
                node = node.next;
            }
        }
        return null;
    }

    public void printInternalArray() {
        System.out.println(Arrays.toString(table));
    }


    private Set<Entry<K, V>> convertToSet() {
        Set<Entry<K, V>> result = new HashSet<>();
        for (Entry<K,V> node : table) {
            if (node != null) {
                do {
                    result.add(node);
                    node = node.next;
                } while (node != null);
            }
        }
        return result;
    }

    @Override
    public String toString() {
        if (size == 0) {
            return "[]";
        }
        StringBuilder builder = new StringBuilder();
        builder.append('[');
        for (Entry<K,V> node : table) {
            if (node != null) {
                do {
                    builder.append(node)
                        .append(", ");
                    node = node.next;
                } while (node != null);
            }
        }
        builder.append("\b\b]");
        return builder.toString();
    }

    protected class Entry<T, U> {
        private final T key;
        private U value;
        private Entry<T, U> next;

        public Entry(T key, U value) {
            this.key = key;
            this.value = value;
        }

        private void setValue(U value) {
            this.value = value;
        }

        private boolean hasNext() {
            return next != null;
        }

        @Override
        public String toString() {
            return "(" + key + ":" + value + ")";
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            @SuppressWarnings("unchecked")
            Entry<?, ?> entry = (Entry<?, ?>) o;
            return Objects.equals(key, entry.key) &&
                    Objects.equals(value, entry.value);
        }

        @Override
        public int hashCode() {
            return Objects.hash(key, value);
        }
    }

    private class EntryEnumerator implements Enumeration<Entry<K, V>> {

        Iterator<Entry<K, V>> entryIterator = convertToSet().iterator();

        @Override
        public boolean hasMoreElements() {
            return entryIterator.hasNext();
        }

        @Override
        public Entry<K, V> nextElement() {
            return entryIterator.next();
        }
    }

    private class ValueEnumerator implements Enumeration<V> {

        Iterator<Entry<K, V>> entryIterator = convertToSet().iterator();

        @Override
        public boolean hasMoreElements() {
            return entryIterator.hasNext();
        }

        @Override
        public V nextElement() {
            return entryIterator.next().value;
        }
    }

    private class KeyEnumerator implements Enumeration<K> {

        Iterator<Entry<K, V>> entryIterator = convertToSet().iterator();

        @Override
        public boolean hasMoreElements() {
            return entryIterator.hasNext();
        }

        @Override
        public K nextElement() {
            return entryIterator.next().key;
        }
    }
}
